//Fichero bot.cpp modificado por GONZALO MARTÍNEZ MILLÁN 52711


#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>

#include "DatosMemCompartida.h"

int main()

{
	
	int file;
	DatosMemCompartida* pMemComp;
	char* proyeccion;

	file=open("datosBot.txt",O_RDWR);
	proyeccion=(char*)mmap(NULL,sizeof(*(pMemComp)),PROT_WRITE|PROT_READ,MAP_SHARED,file,0);

	close(file);


	pMemComp=(DatosMemCompartida*)proyeccion;

	//Acciones de control de la raqueta
	while(1)
	{ 
		float posRaqueta1;
		//Se calculan los centros de las raquetas
		posRaqueta1=((pMemComp->raqueta1.y2+pMemComp->raqueta1.y1)/2);
		if(posRaqueta1 < pMemComp->esfera.centro.y)
			pMemComp->accion1=1;
		else if(posRaqueta1>pMemComp->esfera.centro.y)
			pMemComp->accion1=-1;
		else
			pMemComp->accion1=0;
		
	}

	//Desmontamos la proyeccion de memoria
	munmap(proyeccion,sizeof(*(pMemComp)));

}

