//Fichero logger.cpp modificado por GONZALO MARTÍNEZ MILLÁN 52711



#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

int main(int argc, char* argv[]) {
	mkfifo("fifo",0777);
	int fd=open("fifo", O_RDONLY);
	if(fd < 0){
		perror("Fallo en la apertura de la tuberia");
		return(1);
	}
	int aux;
	char buff[200];
	while(1)
	{
		aux=read(fd,buff,sizeof(buff));
		if(aux < 0){
			perror("Fallo en la lectura");
			return(1);
		}
		if(aux == 0){
			perror("Fin de programa");
			return(1);
		}
		printf("%s\n", buff);

	}
	close(fd);
	unlink("fifo");
	return 0;
}

